import {writable} from "svelte/store";

export const FeedbackStore = writable([
    {
        id: 1,
        rating: 10,
        text: "There are many variations of passages of Lorem Ipsum available," +
            " but the majority have suffered alteration in some form, by injected humour, " +
            "or randomised words which don't look even slightly believable. "
    },
    {
        id: 2,
        rating: 8,
        text: "There are many variations of passages of Lorem Ipsum available," +
            " but the majority have suffered alteration in some form, by injected humour, " +
            "or randomised words which don't look even slightly believable. "
    },
    {
        id: 3,
        rating: 6,
        text: "There are many variations of passages of Lorem Ipsum available," +
            " but the majority have suffered alteration in some form, by injected humour, " +
            "or randomised words which don't look even slightly believable. "
    }
])